import 'package:PluginStube/internal/components/bottomNavigation.dart';
import 'package:PluginStube/internal/objects/post.dart';
import 'package:PluginStube/internal/objects/user.dart';
import 'package:PluginStube/internal/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'internal/routes/profile.dart';

void main() => runApp(MyApp());

final Utils utils = new Utils();

final BottomNav bottomNav = new BottomNav();

final List<Post> posts = _loadPosts();
final User user = new User("@JailBreakerTV", "JailBreakerTV"),
    pischa = new User("@JailBreakerEU", "JailBreakerEU");

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        scaffoldBackgroundColor: Utils.mainColor,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(50.0),
        child: AppBar(
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.apps, color: Utils.blue),
            ),
          ],
          leading: utils.circularImage("assets/images/logo.png", 48, 48),
          centerTitle: true,
          backgroundColor: Utils.mainColor,
        ),
      ),
      body: Container(
        alignment: Alignment.center,
        padding: EdgeInsets.only(right: 10, left: 10),
        child: ListView(
          children: _loadPostWidgets(context),
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        unselectedItemColor: Colors.grey,
        selectedItemColor: Utils.blue,
        backgroundColor: Utils.mainColor,
        onTap: onTabTapped,
        currentIndex: _currentIndex,
        items: bottomNav.getNavigationItems(),
      ),
    );
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
      if (index == 1) {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => Profile()),
        );
      }
    });
  }
}

List<Post> _loadPosts() {
  final List<Post> posts = [];
  for (int i = 0; i < 10; i++) {
    Post post = new Post(user,
        "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At");
    Post post1 = new Post(pischa,
        "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At");
    posts.add(post);
    posts.add(post1);
  }
  return posts;
}

List<Widget> _loadPostWidgets(BuildContext context) {
  List<Widget> widgets = [];

  for (Post post in posts) {
    widgets.add(post);
  }
  return widgets;
}
