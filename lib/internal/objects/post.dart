import 'package:PluginStube/internal/objects/user.dart';
import 'package:PluginStube/internal/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

final Utils utils = new Utils();

class Post extends StatefulWidget {
  User user;
  String text;
  int likes = 0, retweets = 0;
  List<String> comments = [];

  Post(this.user, this.text);

  @override
  _Post createState() => _Post();
}

class _Post extends State<Post> {
  @override
  Widget build(BuildContext context) {
    final Post post = super.widget;
    final User user = post.user;

    final ListTile listTile = new ListTile(
      leading: utils.circularImage("assets/images/logo.png", 48, 48),
      title: Text(
        user.username,
        style: TextStyle(color: Colors.white, fontSize: 20),
      ),
      subtitle: Text(
        user.internname,
        style: TextStyle(color: Colors.white, fontSize: 13),
      ),
    );
    return new InkWell(
      onTap: () {

      },
      child: Card(
        color: Color(0xff14171a),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            listTile,
            new Padding(
              padding: new EdgeInsets.only(left: 50, right: 50),
              child: new Text(
                post.text,
                style: TextStyle(color: Colors.white),
              ),
            ),
            ButtonTheme.bar(
              child: ButtonBar(
                alignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Padding(
                    padding: new EdgeInsets.all(10.0),
                    child: new Row(
                      children: <Widget>[
                        new Padding(
                          padding: new EdgeInsets.only(right: 5),
                          child: Row(
                            children: <Widget>[
                              IconButton(
                                icon: Icon(Icons.chat_bubble_outline),
                                color: Colors.grey,
                                onPressed: () {},
                              ),
                              Text(
                                post.comments.length.toString(),
                                style: TextStyle(
                                  color: Colors.grey,
                                ),
                              )
                            ],
                          ),
                        ),
                        new Padding(
                          padding: new EdgeInsets.only(left: 10),
                          child: Row(
                            children: <Widget>[
                              IconButton(
                                icon: Icon(Icons.autorenew),
                                color: user.hasRetweetet(post)
                                    ? Colors.greenAccent
                                    : Colors.grey,
                                onPressed: () {
                                  if (!user.hasRetweetet(post)) {
                                    Scaffold.of(context).showSnackBar(SnackBar(
                                      content: Text(
                                        "Du hast den Tweet retweetet",
                                        style: TextStyle(color: Colors.white),
                                      ),
                                      backgroundColor: Colors.blueAccent,
                                      duration: Duration(milliseconds: 1100),
                                    ));
                                  }
                                  setState(() {
                                    user.hasRetweetet(post)
                                        ? user.unRetweet(post)
                                        : user.retweet(post);
                                  });
                                },
                              ),
                              Text(
                                post.retweets.toString(),
                                style: TextStyle(
                                  color: user.retweets.contains(post)
                                      ? Colors.greenAccent
                                      : Colors.grey,
                                ),
                              )
                            ],
                          ),
                        ),
                        new Padding(
                          padding: new EdgeInsets.only(left: 10),
                          child: Row(
                            children: <Widget>[
                              IconButton(
                                icon: user.likes.contains(post)
                                    ? Icon(Icons.favorite)
                                    : Icon(Icons.favorite_border),
                                color: user.likes.contains(post)
                                    ? Colors.red
                                    : Colors.grey,
                                onPressed: () {
                                  setState(() {
                                    user.hasLiked(post)
                                        ? user.unLike(post)
                                        : user.like(post);
                                  });
                                },
                              ),
                              Text(
                                post.likes.toString(),
                                style: TextStyle(
                                  color: user.likes.contains(post)
                                      ? Colors.red
                                      : Colors.grey,
                                ),
                              )
                            ],
                          ),
                        ),
                        new Padding(
                          padding: new EdgeInsets.only(left: 10),
                          child: IconButton(
                            icon: Icon(Icons.file_upload),
                            color: Colors.grey,
                            onPressed: () {
                              showModalBottomSheet(
                                context: context,
                                builder: (BuildContext bc) {
                                  return Container(
                                    color: Utils.mainColor,
                                    child: new Wrap(
                                      children: <Widget>[
                                        new ListTile(
                                          leading: new Icon(
                                            Icons.mail_outline,
                                            color: Colors.white,
                                          ),
                                          title: new Text(
                                            'Per Direktnachricht senden',
                                            style: TextStyle(
                                              color: Colors.white,
                                            ),
                                          ),
                                          onTap: () => {},
                                        ),
                                        new ListTile(
                                          leading: new Icon(
                                            Icons.save,
                                            color: Colors.white,
                                          ),
                                          title: new Text(
                                            'Tweet zu Lesezeichen hinzufügen',
                                            style: TextStyle(
                                              color: Colors.white,
                                            ),
                                          ),
                                          onTap: () => {},
                                        ),
                                        new ListTile(
                                          leading: new Icon(
                                            Icons.file_upload,
                                            color: Colors.white,
                                          ),
                                          title: new Text(
                                            'Tweet teilen via...',
                                            style: TextStyle(
                                              color: Colors.white,
                                            ),
                                          ),
                                          onTap: () => {},
                                        )
                                      ],
                                    ),
                                  );
                                },
                              );
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
