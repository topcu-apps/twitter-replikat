import 'package:PluginStube/internal/objects/post.dart';

class User {
  String username;
  String internname;
  List<Post> posts = [], likes = [], retweets = [];

  User(this.internname, this.username);

  void addPost(Post post) {
    this.posts.add(post);
  }

  bool hasLiked(Post post) {
    return likes.contains(post);
  }

  bool hasRetweetet(Post post) {
    return retweets.contains(post);
  }

  void unLike(Post post) {
    likes.remove(post);
    post.likes--;
  }

  void unRetweet(Post post) {
    retweets.remove(post);
    post.retweets--;
  }

  void like(Post post) {
    likes.add(post);
    post.likes++;
  }

  void retweet(Post post) {
    retweets.add(post);
    post.retweets++;
  }
}
