import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class BottomNav {
  List<BottomNavigationBarItem> getNavigationItems() {
    return [
      BottomNavigationBarItem(
          icon: Icon(
            Icons.home,
          ),
          title: Text("Start")),
      BottomNavigationBarItem(
          icon: Icon(
            Icons.search,
          ),
          title: Text("Suchen")),
      BottomNavigationBarItem(
        icon: Icon(
          Icons.notifications_none,
        ),
        activeIcon: Icon(Icons.notifications),
        title: Text("Notifications"),
      ),
      BottomNavigationBarItem(
          icon: Icon(
            Icons.mail_outline,
          ),
          activeIcon: Icon(Icons.mail),
          title: Text("Messages")),
    ];
  }
}
