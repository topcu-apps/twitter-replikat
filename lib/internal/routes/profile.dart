import 'package:PluginStube/internal/components/bottomNavigation.dart';
import 'package:PluginStube/internal/utils.dart';
import 'package:PluginStube/main.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

final Utils utils = new Utils();

final BottomNav bottomNav = new BottomNav();

class Profile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        scaffoldBackgroundColor: Utils.mainColor,
      ),
      home: ProfilePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class ProfilePage extends StatefulWidget {
  ProfilePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  int _currentIndex = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(50.0),
        child: AppBar(
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.apps, color: Utils.blue),
            ),
          ],
          leading: utils.circularImage("assets/images/logo.png", 48, 48),
          centerTitle: true,
          backgroundColor: Utils.mainColor,
        ),
      ),
      body: Container(
        alignment: Alignment.center,
        padding: EdgeInsets.only(right: 10, left: 10),
        child: Center(
          child: Text(
            "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",
            style: TextStyle(color: Colors.white),
          ),
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        unselectedItemColor: Colors.grey,
        selectedItemColor: Utils.blue,
        backgroundColor: Utils.mainColor,
        onTap: onTabTapped,
        currentIndex: _currentIndex,
        items: bottomNav.getNavigationItems(),
      ),
    );
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });



    switch (index) {
      case 0:
        break;
      case 1:
        break;
      case 2:
        break;
      case 3:
        break;
    }

    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => MyHomePage()),
    );
  }
}
