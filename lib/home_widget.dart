import 'package:flutter/material.dart';

class BanFormular extends StatefulWidget {
  BanFormular({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _BanFormularState createState() => _BanFormularState();
}

class _BanFormularState extends State<BanFormular> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          TextFormField(
            maxLength: 16,
            decoration: InputDecoration(
                icon: Icon(Icons.account_circle), helperText: "Username"),
            validator: (value) {
              if (value.isEmpty) {
                return 'Gib einen Nutzernamen an';
              }
              return null;
            },
          ),
          TextFormField(
            maxLength: 30,
            decoration:
                InputDecoration(icon: Icon(Icons.chat), helperText: "Grund"),
            validator: (value) {
              if (value.isEmpty) {
                return 'Gib einen Grund für die Bestrafung an';
              }
              return null;
            },
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16.0),
            child: RaisedButton(
              onPressed: () {
                if (_formKey.currentState.validate()) {
                  Scaffold.of(context).showSnackBar(SnackBar(
                      content: Text('Versende Informationen'),
                      backgroundColor: Colors.green));
                } else {
                  Scaffold.of(context).showSnackBar(SnackBar(
                      content: Text('Bitte fülle das Formular aus'),
                      backgroundColor: Colors.red));
                }
              },
              color: Colors.green,
              child: Center(
                child: const Text('Nutzer bannen',
                    style: TextStyle(fontSize: 20, color: Colors.white)),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
